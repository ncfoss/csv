/*    License: Unlicense  (No rights reserved!)
 *       NOTE: Should parse MOST CSVs
 * Separators: ,;\t
 */
;function csvParse(aCsv, aCSep, aSSep) {
	aSSep = aSSep || '"';
	var sep = aCSep || { ",": 1, ";": 1, "\t": 1 },
		lines = aCsv.split(/[\r\n]|[\n\r]|\r|\n/);
	// parse lines
	for (var i = 0, llen = lines.length; i < llen; i++) {
		if (lines[i].split)
			lines[i] = parseLine(lines[i]);
	}
	// func(s)
	function parseLine(line) {
		for (var ret = [], i = 0, bqs, iQuote, iqs, t = "", slen = line.length; i < slen; i++) {
			var cc = line.charAt(i),
				nc = line.charAt(1 + i),
				curSS = cc === aSSep,
				nxtSS = nc === aSSep;
			// etc
			if (iQuote) {
				// in q
				if (t && iqs) iqs = 0, t = t.trim();
				if (curSS && nxtSS)
					t += aSSep, i++;
				else if (curSS) {
					if (bqs) t+=cc;
					iQuote = 0;
				} else t += cc;
			} else {
				// not in q
				if (sep[cc]) {
					if (t.length)
						ret.push(t);
					t = "";
					bqs = 0;
				} else {
					if (curSS) {
						if (t.length) bqs=1, t+=cc;
						iQuote = iqs = 1;
					} else {
						t += cc;
					}
				}
			}
		}
		if (t.length) ret.push(t);
		return ret;
	}
	return lines;
}
function csvStringify(aArray, aCSep, aSSep) {
	aSSep = aSSep || '"', aCSep = aCSep || ",";
	for (var i in aArray) {
		for (var i2 in aArray[i])
			if (1 + aArray[i][i2].indexOf(aSSep))
				aArray[i][i2] = aSSep + aArray[i][i2].split(aSSep).join(aSSep + aSSep) + aSSep;
		aArray[i] = aArray[i].join(aCSep);
	}
	return aArray.join("\r\n");
};
