# CSV Parser
### This is free and unencumbered software released into the public domain.
<img src="https://api.visitorbadge.io/api/visitors?path=gitlab.ncfoss.csv&countColor=%2337d67a&style=flat-square&labelStyle=upper"><br>
## CAUTION & Features:
- Column separators: `,`, `;`, and `\t`.
- Line parser done with:
    - RegEx: `/[\r\n]|[\n\r]|\r|\n/`
- SHOULD be able to parse most CSVs.

## Usage
See <a href="https://gitlab.com/ncfoss/csv/-/blob/main/src/js/csv.js" target="_blank">csv.js</a>, or minified: <a href="https://gitlab.com/ncfoss/csv/-/blob/main/src/js/csv.min.js" target="_blank">csv.min.js</a><br>

JS, Definition:<br>
```js
// Definitions:
csvParse(aCsv: String[, aCSep: Object, aSSep: String]);
csvStringify(aArray[, aCSep: String, aSSep: String]);
```
JS, Simple Usage:
```js
// Here's some csv that works:
var arr = csvParse('a,b,c\r\rd,e,f\n\r"a b","c ""d",e,f\r\n');
console.log(arr);
// Now to stringify:
var csv = csvStringify(arr);
console.log(csv);
```

JS, Full potential usage:
```js
// Here we set column separators to be "!" and ","
// Then we set string separator to be apos(')
var columnSeparators =  {",":1,"!":1},
    stringSeparator = "'",
    exports_ColumnSeparator = ",";
var arr = csvParse(" a,b!c, 'aa''b'", columnSeparators, stringSeparator);

// Now we export the csv
var csv = csvStringify(arr, exports_ColumnSeparator, stringSeparator);

// etc
console.log(arr, csv);

```



## License
Unlicense.
